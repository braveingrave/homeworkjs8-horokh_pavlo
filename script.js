// 1
document.querySelectorAll('p').forEach(e => e.style.backgroundColor = "#ff0000");
// 2
let optList = document.getElementById("optionsList");
console.log(optList);
console.log(optList.parentNode)
optList.childNodes.forEach(e => {console.log(e.nodeName, e.nodeType)});
//3
let addP = document.getElementById("testParagraph");
addP.innerHTML = "This is a paragraph";
//4
let mainElem = document.getElementsByClassName('main-header')[0];
console.log(mainElem.childNodes)
mainElem.childNodes.forEach (e => { if (e.nodeType === 1) e.classList.add('nav-item')})
//5(5-6?)
let seekClass = document.querySelector('.section-title');
seekClass.classList.remove('section-title');